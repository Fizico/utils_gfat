#!/usr/bin/env python
# -*- coding: utf8 -*-

"""
utilities for logging
"""

from __future__ import print_function
from __future__ import division
from __future__ import absolute_import

import sys
import os
import logging.config

from . import utils

LOG_FMT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOG_DATE_FMT = '%Y-%m-%d %H:%M:%S'
LOG_DIR = 'logs'
LOG_LEVELS = ['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL']
LOG_LEVELS_INT = {
    1: 'CRITICAL',
    2: 'WARNING',
    3: 'DEBUG',
}


def init(log_name, log_file, log_file_level, int_log_level):
    """
    Configure the logger and start it
    """

    # check input
    if int_log_level != 0:
        log_file_level = LOG_LEVELS_INT[int_log_level]

    log_cmd_level = log_file_level

    # Check the logs directory
    dir_ = os.path.dirname(log_file)
    file_ = os.path.basename(log_file)
    dir_ok = utils.check_dir(dir_)
    if not dir_ok:
        print('directory {} does not exist. Creating it'.format(dir_))
        try:
            os.makedirs(dir_)
        except OSError:
            print('failed to create {}'.format(dir_))
            print('quitting {}'.format(log_name))
            sys.exit(1)

    print('debug file : {}'.format(file_))
    print('console debug level : {}'.format(log_cmd_level))
    print('file debug level : {}'.format(log_file_level))

    log_dict = {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "simple": {
                "datefmt": LOG_DATE_FMT,
                "format": LOG_FMT,
            }
        },

        "handlers": {
            "console": {
                "class": "logging.StreamHandler",
                "level": log_cmd_level,
                "formatter": "simple",
                "stream": "ext://sys.stdout"
            },
            "file_handler": {
                "class": "logging.handlers.RotatingFileHandler",
                "level": log_file_level,
                "formatter": "simple",
                "filename": os.path.join(dir_, file_),
                "maxBytes": 10485760,
                "backupCount": 10,
                "encoding": "utf8"
            }
        },

        "root": {
            "level": 'DEBUG',
            "handlers": ["console", "file_handler"]
        }
    }

    logger = logging.getLogger(log_name)
    logging.config.dictConfig(log_dict)

    return logger
