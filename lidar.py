#!/usr/bin/env python

import os
import sys
import glob
import argparse
import matplotlib
import scipy as sp
import numpy as np
import pandas as pd
import xarray as xr
import scipy.ndimage
import netCDF4 as nc
import datetime as dt
from utils_gfat import plot
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from distutils.dir_util import mkpath
from matplotlib.dates import DateFormatter

__author__ = "Bravo-Aranda, Juan Antonio"
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Bravo-Aranda, Juan Antonio"
__email__ = "jabravo@ugr.es"
__status__ = "Production"


def getTP(filepath):    
    """
    Get temperature and pressure from header of a licel-formatted binary file. 
    Inputs:
    - filepath: path of a licel-formatted binary file (str)    
    Output:
    - temperature: temperature in celsius (float).
    - pressure: pressure in hPa (float).
    """
    #This code should evolve to read the whole header, not only temperature and pressure. 
    if os.path.isfile(filepath):        
            with open(filepath, mode="rb") as f: #
                filename = f.readline().decode('utf-8').rstrip()[1:]
                second_line = f.readline().decode('utf-8')
                f.close()            
            temperature=float(second_line.split(' ')[12].rstrip())
            pressure=float(second_line.split(' ')[13].rstrip())
    else:
        print('File not found.')
        temperature = None
        pressure = None
    return temperature, pressure

def merge_polarized_channels(lxarray):
    """
    It merges the polarized channels and retrieve the Linear Volume Depolarization Ratio
    Input :
    lxarray: xarray.Dataset from lidar.preprocessing() (xarray.Dataset)
    Output:  
    lxarray: xarray.Dataset with new varaibles ('signal%d_total' % _wavelength ; 'LVDR%d' % _wavelength)
    """
    
    R = 1 #Reflected
    T = 2 #Transmitted
    analog = 0
    photoncounting = 1
    
    #Dictionary organized by wavelengths
    depoCalib={}
    #Each wavelength has a depolarization calibration DataFrame
    depoCalib['532'] = pd.DataFrame(columns=['date', 'eta_an', 'eta_pc', 'GR', 'HR', 'GT', 'HT', 'K'])
    dict0 = {'date': dt.datetime(2015,6,7), 'eta_an': 0.0757, 'eta_pc': 0.1163, 'GR':1., 'HR':1., 'GT':1, 'HT':-1., 'K': 1.}
    dict1 = {'date': dt.datetime(2017,9,1), 'eta_an': 0.041, 'eta_pc': 0.0717, 'GR':1.31651, 'HR':1.22341, 'GT':0.65366, 'HT':-0.62378, 'K': 1.007}
    dict2 = {'date': dt.datetime(2019,1,1), 'eta_an': 0.041, 'eta_pc': 0.0717, 'GR':1.31651, 'HR':1.22341, 'GT':0.65366, 'HT':-0.62378, 'K': 1.007}
#     dict2 = {'date': dt.datetime(2019,7,1), 'eta_an': 0.041, 'eta_pc': 0.0717, 'GR':1.31651, 'HR':1., 'GT':1., 'HT':-1., 'K': 1.}
    depoCalib['532'].loc[0] = pd.Series(dict0)
    depoCalib['532'].loc[1]= pd.Series(dict1)
    depoCalib['532'].loc[2]= pd.Series(dict2)
    depoCalib['532'] = depoCalib['532'].set_index('date')
    
    #Date of the current measurement
    current_date = lxarray.time[0].min().values
    
    #Consider splitted channels (i.e., it must have Reflected and Transmitted channels)
    wavelengths = np.unique(lxarray['wavelength'].values[np.logical_or(lxarray['polarization'].values == R, lxarray['detection_mode'].values == T)])
    for _wavelength in wavelengths:
        #Search the last calibration performed the current measurement
        idx = depoCalib['%0d' % _wavelength].index.get_loc(current_date, method='pad') #'pad': search the nearest lower; 'nearest': search the absolute nearest.
        calib = depoCalib['%0d' % _wavelength].iloc[idx]
        # print(calib)
        
        #Find the signals to be used: wavelength | reflected/transmitted | analog/photoncounging
        wave_T_an = np.argwhere(np.logical_and.reduce((lxarray['polarization'].values == T, lxarray['detection_mode'].values == analog, lxarray['wavelength'].values == _wavelength)))        
        signal_Tan = lxarray['corrected_rcs_%02d' % wave_T_an]/np.power(lxarray['range'],2)
        wave_T_pc = np.argwhere(np.logical_and.reduce((lxarray['polarization'].values == T, lxarray['detection_mode'].values == photoncounting, lxarray['wavelength'].values == _wavelength)))        
        signal_Tpc = lxarray['corrected_rcs_%02d' % wave_T_pc]/np.power(lxarray['range'],2)
        wave_R_an = np.argwhere(np.logical_and.reduce((lxarray['polarization'].values == R, lxarray['detection_mode'].values == analog, lxarray['wavelength'].values == _wavelength)))        
        signal_Ran = lxarray['corrected_rcs_%02d' % wave_R_an]/np.power(lxarray['range'],2)
        wave_R_pc = np.argwhere(np.logical_and.reduce((lxarray['polarization'].values == R, lxarray['detection_mode'].values == photoncounting, lxarray['wavelength'].values == _wavelength)))        
        signal_Rpc = lxarray['corrected_rcs_%02d' % wave_R_pc]/np.power(lxarray['range'],2)    
        
        #Total signal
        lxarray['total_rcs%d_an' % _wavelength] = (calib['eta_an']*calib['HR']*signal_Tan - calib['HT']*signal_Ran)*np.power(lxarray['range'],2)
        lxarray['total_rcs%d_an' % _wavelength].attrs['long_name'] = 'Range corrected signal'
        lxarray['total_rcs%d_an' % _wavelength].attrs['wavelength'] = _wavelength
        lxarray['total_rcs%d_an' % _wavelength].attrs['detection_mode'] = 0
        lxarray['total_rcs%d_an' % _wavelength].attrs['units'] = 'a.u.'
        lxarray['total_rcs%d_pc' % _wavelength] = (calib['eta_pc']*calib['HR']*signal_Tpc - calib['HT']*signal_Rpc)*np.power(lxarray['range'],2)
        lxarray['total_rcs%d_pc' % _wavelength].attrs['long_name'] = 'Range corrected signal'
        lxarray['total_rcs%d_pc' % _wavelength].attrs['wavelength'] = _wavelength
        lxarray['total_rcs%d_pc' % _wavelength].attrs['detection_mode'] = 1
        lxarray['total_rcs%d_pc' % _wavelength].attrs['units'] = 'a.u.'
        
        #LVDR
        LVDR={}
        eta_an = calib['eta_an']/calib['K']
        ratio_an = (signal_Ran/signal_Tan)/eta_an
        LVDR['an'] = ((calib['GT']+calib['HT'])*ratio_an - (calib['GR']+calib['HR']))/((calib['GR']-calib['HR']) - (calib['GT']-calib['HT'])*ratio_an)
        lxarray['LVDR%d_an' % _wavelength] = LVDR['an']
        lxarray['LVDR%d_an' % _wavelength].attrs['long_name'] = 'Linear Volume Depolarization Ratio'
        lxarray['LVDR%d_an' % _wavelength].attrs['detection_mode'] = 0
        lxarray['LVDR%d_an' % _wavelength].attrs['wavelength'] = _wavelength
        lxarray['LVDR%d_an' % _wavelength].attrs['units'] = '#'
        eta_pc = calib['eta_pc']/calib['K']
        ratio_pc = (signal_Rpc/signal_Tpc)/eta_pc
        LVDR['pc'] = ((calib['GT']+calib['HT'])*ratio_pc - (calib['GR']+calib['HR']))/((calib['GR']-calib['HR']) - (calib['GT']-calib['HT'])*ratio_pc)
        lxarray['LVDR%d_pc' % _wavelength] = LVDR['pc']
        lxarray['LVDR%d_pc' % _wavelength].attrs['long_name'] = 'Linear Volume Depolarization Ratio'
        lxarray['LVDR%d_pc' % _wavelength].attrs['detection_mode'] = 1
        lxarray['LVDR%d_pc' % _wavelength].attrs['wavelength'] = _wavelength
        lxarray['LVDR%d_pc' % _wavelength].attrs['units'] = '#'    
        
        print(lxarray['LVDR532_an'].attrs['long_name'])
        print(lxarray['LVDR532_pc'].attrs['long_name'])
    return lxarray

def reader_netcdf(filelist, channels):
    """
    Lidar data reader. 
    Inputs:
    - filelist: List of radar files (i.e, '/drives/c/*.nc') (str)
    - channels: tuple of channel numbers (e.g., (0,1,2) ) (tuple)
    Output:
    - lidar: dictionary or 'None' in case of error.
    """
    
    #Date format conversion
    files2load = glob.glob(filelist)    

    if files2load:        
        lidar = {}

        # open all files
        nc_ids = [nc.Dataset(file_) for file_ in files2load]

        # localization of instrument
        lidar['lat'] = nc_ids[0].variables['lat'][:]
        lidar['lon'] = nc_ids[0].variables['lon'][:]
        lidar['alt'] = nc_ids[0].variables['altitude'][:]
        lidar['location'] = nc_ids[0].site_location.split(',')[0]
        lidar['instr'] = nc_ids[0].system

        # read alt (no need to concantenate)
        lidar['range'] = nc_ids[0].variables['range'][:]

        # wavelength (no need to concantenate)
        tmp = nc_ids[0].variables['wavelength'][:]
        lidar['wavelength'] = tmp 
        lidar['wavelength_units'] = nc_ids[0].variables['wavelength'].units  

        # detection_mode (no need to concantenate)
        tmp = nc_ids[0].variables['detection_mode'][:]
        lidar['detection_mode'] = tmp

        # polarization (no need to concantenate)
        tmp = nc_ids[0].variables['polarization'][:]
        lidar['polarization'] = tmp

        # read time 
        units = nc_ids[0].variables['time'].units #'days since %s' % dt.datetime.strftime(date, '%Y-%m-%d %H:%M:%S')
        tmp = [nc.num2date(nc_id.variables['time'][:], units) for nc_id in nc_ids]
        lidar['raw_time'] = np.concatenate(tmp)

        # check if any data available        
        # time_filter = (lidar['raw_time'] >= date) & (lidar['raw_time'] < date + dt.timedelta(days=1))
        # if not np.any(time_filter):
        #     print('No data in user-defined time.')
        #     return None

        # RCS
        for channel in channels:            
            tmp = [nc_id.variables['rcs_%02d' % channel][:] for nc_id in nc_ids]
            lidar['rcs_%02d' % channel] = np.ma.filled(np.concatenate(tmp, axis=0))    

            # Background
            tmp = [nc_id.variables['bckgrd_rcs_%02d' % channel][:] for nc_id in nc_ids]
            lidar['background_%02d' % channel] = np.ma.filled(np.concatenate(tmp, axis=0), fill_value=np.nan)       
        
        # close all files
        [nc_id.close() for nc_id in nc_ids]        
    else:
        lidar = None        
    return lidar

def reader_xarray(filelist):
    """
    Lidar data reader using xarray module. 
    Inputs:
    - filelist: List of lidar files (i.e, '/drives/c/*.nc') (str)    
    Output:
    - lidar: dictionary or 'None' in case of error.
    """
    
    #Date format conversion
    files2load = glob.glob(filelist)    

    if files2load:        
        lidartemp = []
        lidartemp = xr.open_mfdataset(files2load)
        lidar = lidartemp.sortby(lidartemp['time'])
        lidartemp.close()

        #Signal conversion
        for channel_ in lidar.n_chan.values:            
            lidar['signal_%02d' % channel_] = lidar['rcs_%02d' % channel_]/lidar['range']**2
            lidar['bckgrd_signal_%02d' % channel_] = lidar['bckgrd_rcs_%02d' % channel_] 
            #Units depending on analog or photoncounting channel
            # idx = np.isfinite(lxarray.wavelength.values[:, channel_])                
            # detectionmode = np.unique(lidartemp.detection_mode.values[idx, channel_])
            # print(lidar.detection_mode.values[channel_])
            # if lidar.detection_mode[channel_]:
            #     lidar['signal_%02d' % channel_].attrs['units'] = 'MHz'
            #     lidar['bckgrd_signal_%02d' % channel_].attrs['units'] = 'MHz'
            # else:
            #     lidar['signal_%02d' % channel_].attrs['units'] =  'mV'
            #     lidar['bckgrd_signal_%02d' % channel_].attrs['units'] =  'mV'        
        lidar.attrs['BCK_MIN_ALT'] = 75000       
        lidar.attrs['BCK_MAX_ALT'] = 105000

        #Extract information from filename        
        lidar.attrs['lidarNick'] = os.path.basename(files2load[0]).split('_')[0]
        lidar.attrs['dataversion'] = os.path.basename(files2load[0]).split('_')[1]
    else:
        lidar=None
        print('No files found with the format: %s' % filelist)
    return lidar

def preprocessing(filelist, dcfilelist):
    """
    Preprocessing lidar signals including: dead time, dark measurement, background, and bin shift.
    Input:
    - filelist: List of lidar files (i.e, '/drives/c/*.nc') (str)    
    - dcfilelist: List of DC lidar files (i.e, '/drives/c/*.nc') (str)    
    Output:
    - lidar: dictionary or 'None' in case of error.
    """

    lxarray = reader_xarray(filelist)        
    if lxarray != None:
        #Read dark measurements
        dclidar = reader_xarray(dcfilelist)
        for channel_ in lxarray.n_chan.values: #np.asarray((0,)):                
            if np.isfinite(lxarray.wavelength.values[channel_]):
                if lxarray.detection_mode.values[channel_] == 0:
                    #Regular signal
                    signal = lxarray['signal_%02d' % channel_].values

                    #Background signal
                    bckgrd_signal = lxarray['bckgrd_signal_%02d' % channel_].values
                    
                    if dclidar != None:
                        if lxarray.n_chan.size == dclidar.n_chan.size:
                            #Smooth DC signals
                            #DC measurement
                            dc = dclidar['signal_%02d' % channel_].values

                            #Smooth module
                                #DC weights
                            weights = np.array([1, 2, 4, 6, 8, 6, 4, 2, 1], dtype=np.float)
                            weights = weights / np.sum(weights[:])     
                                #Smooth
                            smdc = sp.ndimage.filters.convolve(dc.mean(axis=0), weights, mode='constant')                

                            #Substraction Dark Measurement to Regular signal
                            matrix_smdc = np.tile(smdc, (lxarray.time.size,1))                                                            
                            dc_signal = signal - matrix_smdc

                            #Substraction Dark Measurement to Background signal
                            bck_filter = (lxarray['range'] > lxarray.attrs['BCK_MIN_ALT']) & (lxarray['range'] < lxarray.attrs['BCK_MAX_ALT'])        
                            bck_smdc = smdc[bck_filter].mean()
                            array_bck_smdc = np.tile(bck_smdc, (np.size(bckgrd_signal)))                                                            
                            dc_bckgrd_signal = bckgrd_signal - array_bck_smdc
                            current_long_name = 'DM-, background- and range-corrected signal'
                        else:
                            #DC correction NOT applied to analogchannels
                            print('Warning: number of channels of dark and regular measurements do not match.')
                            print('Process continues without dark measurment correction.')
                            dc_signal = signal
                            dc_bckgrd_signal = bckgrd_signal
                            current_long_name = 'background- and range-corrected signal'
                    else:
                        #DC correction NOT applied to analogchannels
                        print('ERROR: lidar files not found: %s' % filelist)
                        print('Warning: dark measurement files not found: %s' % dcfilelist)
                        print('Process continues without dark measurment correction.')
                        dc_signal = signal
                        dc_bckgrd_signal = bckgrd_signal
                        current_long_name = 'background- and range-corrected signal'                        
                    #Substraction Background Measurement        
                    mbckg = np.tile(dc_bckgrd_signal, (lxarray.range.size,1)).T
                    bckg_dc_signal = dc_signal - mbckg

                    #Convert to RCS
                    mrange = np.tile(np.square(lxarray.range), (lxarray.time.size,1))
                    corrected_rcs = bckg_dc_signal * mrange                        
                else:                        
                    #Dead-time correction only applied to photoncounting channels:
                    #Dead-time value
                    deadtime_value = 270.
                    
                    #Regular signal
                    signal = lxarray['signal_%02d' % channel_].values
                    
                    #Correction
                    dt_array = deadtime_value * np.ones(np.shape(signal))
                    dt_signal = signal * (dt_array/(dt_array - signal))  
                    
                    #Background signal
                    bckgrd_signal = lxarray['bckgrd_signal_%02d' % channel_].values

                    #Substraction Background Measurement        
                    mbckg = np.tile(bckgrd_signal, (lxarray.range.size,1)).T
                    bckg_dt_signal = dt_signal - mbckg
                    
                    #Convert to RCS
                    mrange = np.tile(np.square(lxarray.range), (lxarray.time.size,1))
                    corrected_rcs = bckg_dt_signal * mrange
                    current_long_name = 'DT-, background- and range-corrected signal'
            
                #Include new variable in the Dataset
                lxarray['corrected_rcs_%02d' % channel_] = ( ('time', 'range'), corrected_rcs)
                lxarray['corrected_rcs_%02d' % channel_].attrs['units'] = 'a.u.'
                lxarray['corrected_rcs_%02d' % channel_].attrs['long_name'] = current_long_name
        
        #Depolarization channel        
        lxarray = merge_polarized_channels(lxarray)                        
    else:
        lxarray=None        
    return lxarray    

def plot_lidar_channels(filelist, dcfilelist, channels2plot, plt_conf, figdirectory, plot_depo=True):
    """
    Quicklook maker of lidar measurements.
    Inputs:
    - filelist: List of radar files (i.e, '/drives/c/*ZEN*.LC?') (str).
    - dcfilelist: List of dark measurement radar files (i.e, '/drives/c/*ZEN*.LC?') (str).
    - channels2plot: Array of numbers corresponding to the lidar channels (integer).
    - plt_conf: dictionary with plot configuration (dict).
    - figdirectory: directory to save the figure. Date tree will be created (str).
    Outputs:
    - None
    """
    #Dictionaries
    #Detection mode (analog, photoncounting)
    mode={0:'a', 1: 'p'}
    #Polarization component (total, parallel, cross)
    pol={0:'t', 1: 'p', 2:'c'}    
    
    #Font size of the letters in the figure
    matplotlib.rcParams.update({'font.size': 16})
    
    # Read the list of files to plot
    # --------------------------------------------------------------------
    lxarray = preprocessing(filelist, dcfilelist)

    if lxarray != None:
        ldate = lxarray['time'].values[0]

        if ldate <= np.datetime64('2019-10-15'):
            #Mininum value on the colorbar
            Vmin = {0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0}
            #Maximum value on the colorbar
            Vmax = {0: 1e9, 1: 5e10, 2: 5e8, 3: 5e8, 4: 1e9, 5: 5e8, 6: 1e9, 7: 5e8, 8: 1e7, 9: 5e8}        
        else:            
            #Mininum value on the colorbar
            Vmin = {0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0}
            #Maximum value on the colorbar
            Vmax = {0: 1e9, 1: 5e10, 2: 5e8, 3: 5e8, 4: 1e9, 5: 5e8, 6: 1e9, 7: 5e8, 8: 1e7, 9: 5e8}        

        # One figure per variable
        # --------------------------------------------------------------------
        for channel_ in channels2plot:
            print('Current plot %02d' % channel_)
            print('Colorbar range: %f - %f' % (Vmin[channel_], Vmax[channel_]))
            #Create channel string 
            modestr = mode[np.int(lxarray.detection_mode[channel_].values)]
            polstr = pol[np.int(lxarray.polarization[channel_].values)]
            wave = lxarray.wavelength.values[channel_].astype('int').astype('str')
            channelstr = '%sx%s%s' % (wave, polstr, modestr)

            #Create Figure
            fig = plt.figure(figsize=(15,5))
            axes = fig.add_subplot(111)    
            #Plot        
            cmap = matplotlib.cm.jet
            bounds = np.linspace(Vmin[channel_], Vmax[channel_], 128)
            norm = matplotlib.colors.BoundaryNorm(bounds, cmap.N)
            range_km = lxarray.range/1000.
            q = axes.pcolormesh(lxarray.time,range_km, lxarray['corrected_rcs_%02d' % channel_].T,
                                cmap=cmap,
                                vmin=Vmin[channel_],
                                vmax=Vmax[channel_])
            q.cmap.set_over('white')
            cb = plt.colorbar(q, ax=axes,
                            #ticks=bounds,
                            extend='max',
                            format='%.1e')
                    
            # search for gaps in data
            # --------------------------------------------------------------------     
            if plt_conf['gapsize'] == 'default':        
                dif_time = lxarray['time'].values[1:] - lxarray['time'].values[0:-1]                   
                GAP_SIZE = 2*int(np.ceil((np.median(dif_time).astype('timedelta64[s]').astype('float')/60))) #GAP_SIZE is defined as the median of the resolution fo the time array (in minutes)        
                print('GAP_SIZE parameter automatically retrieved to be %d.' % GAP_SIZE)         
            else:
                GAP_SIZE = int(plt_conf['gapsize'])
                print('GAP_SIZE set by the user: %d (in minutes)' % GAP_SIZE)               
            dttime = np.asarray([dt.datetime.utcfromtimestamp((time_ - np.datetime64('1970-01-01T00:00:00Z')) / np.timedelta64(1, 's')) for time_ in  lxarray['time'].values]   )
            plot.gapsizer(plt.gca(),dttime, lxarray['range'], GAP_SIZE, '#c7c7c7')  

            # Setting axes 
            # --------------------------------------------------------------------
            mf = matplotlib.ticker.FuncFormatter(plot.tmp_f)
            axes.xaxis.set_major_formatter(mf)
            hours = mdates.HourLocator(range(0, 25, 3))
            date_fmt = mdates.DateFormatter('%H')
            axes.xaxis.set_major_locator(hours)
            axes.xaxis.set_major_formatter(date_fmt)        
            min_date = lxarray['time'].values.min()
            max_date = lxarray['time'].values.max()
            axes.set_xlim(min_date.astype('datetime64[D]'), max_date.astype('datetime64[D]') + np.timedelta64(1,'D'))            
            axes.set_ylim(plt_conf['y_min'], plt_conf['y_max'])
            plt.grid(True)
            axes.set_xlabel('Time, $[UTC]$')
            axes.set_ylabel('Altitude, $[km]$')
            cb.ax.set_ylabel('Range corrected signal, a.u.')
                
            # title
            # ----------------------------------------------------------------------------
            datestr = lxarray.time[0].min().values.astype('str').split('T')[0]
            plt_conf['title1'] = '%s %s' % (lxarray.attrs['instrument_id'], channelstr)
            plot.title1(plt_conf['title1'], plt_conf['coeff'])
            plot.title2(datestr, plt_conf['coeff'])
            plot.title3('{} ({:.1f}N, {:.1f}E)'.format(lxarray.attrs['site_location'], float(lxarray['lat'].values), float(lxarray['lon'].values)), plt_conf['coeff'])

            # logo
            # ----------------------------------------------------------------------------
            plot.add_GFAT_logo([0.85, 0.01, 0.15, 0.15])

            debugging = False

            if debugging:
                plt.show()
            else:            
                #create output folder
                # --------------------------------------------------------------------
                year = datestr[0:4]
                fulldirpath = os.path.join(figdirectory, channelstr, year) 
                if np.logical_not(os.path.exists(fulldirpath)):
                    mkpath(fulldirpath)
                    print('fulldirpath created: %s' % fulldirpath)                
                figstr = '%s_%s_rcs-%s_%s.png' % (lxarray.attrs['lidarNick'], lxarray.attrs['dataversion'], channelstr, datestr.replace('-',''))
                finalpath = os.path.join(fulldirpath, figstr)
                print('Saving %s' % finalpath)
                plt.savefig(finalpath, dpi=100, bbox_inches = 'tight')
                if os.path.exists(finalpath):
                    print('Saving %s...DONE!' % finalpath)
                else:
                    print('Saving %s... error!' % finalpath)
                plt.close()

        # print('Depolarization and total part')

        #plot depolarization
        for _key in lxarray.keys():                  
            if '532_an' in _key:  
                Vmin={'total_rcs532_an':0. , 'LVDR532_an': 0.}
                Vmax={'total_rcs532_an':1e9 , 'LVDR532_an': 0.4}                
                channels={'total_rcs532_an':'532xta' , 'LVDR532_an': '532xda'}
                signals={'total_rcs532_an':'rcs' , 'LVDR532_an': 'lvdr'}
                print('Current plot %s' % _key)            
                #Create channel string 
                _mode = lxarray[_key].attrs['detection_mode']   
                wave = lxarray[_key].attrs['wavelength']                
                print('%s' % channels[_key])

                #Create Figure
                fig = plt.figure(figsize=(15,5))
                axes = fig.add_subplot(111)    
                #Plot
                cmap = matplotlib.cm.jet
                bounds = np.linspace(Vmin[_key], Vmax[_key], 128)
                norm = matplotlib.colors.BoundaryNorm(bounds, cmap.N)
                range_km = lxarray.range/1000.
                q = axes.pcolormesh(lxarray.time,range_km, lxarray[_key].T,
                                    cmap=cmap,
                                    vmin=Vmin[_key],
                                    vmax=Vmax[_key])
                q.cmap.set_over('white')
                cb = plt.colorbar(q, ax=axes,
                                #ticks=bounds,
                                extend='max',
                                format='%.1e')
                        
                # search for gaps in data
                # --------------------------------------------------------------------     
                if plt_conf['gapsize'] == 'default':        
                    dif_time = lxarray['time'].values[1:] - lxarray['time'].values[0:-1]                   
                    GAP_SIZE = 2*int(np.ceil((np.median(dif_time).astype('timedelta64[s]').astype('float')/60))) #GAP_SIZE is defined as the median of the resolution fo the time array (in minutes)        
                    print('GAP_SIZE parameter automatically retrieved to be %d.' % GAP_SIZE)         
                else:
                    GAP_SIZE = int(plt_conf['gapsize'])
                    print('GAP_SIZE set by the user: %d (in minutes)' % GAP_SIZE)               
                dttime = np.asarray([dt.datetime.utcfromtimestamp((time_ - np.datetime64('1970-01-01T00:00:00Z')) / np.timedelta64(1, 's')) for time_ in  lxarray['time'].values]   )
                plot.gapsizer(plt.gca(),dttime, lxarray['range'], GAP_SIZE, '#c7c7c7')  

                # Setting axes 
                # --------------------------------------------------------------------
                mf = matplotlib.ticker.FuncFormatter(plot.tmp_f)
                axes.xaxis.set_major_formatter(mf)
                hours = mdates.HourLocator(range(0, 25, 3))
                date_fmt = mdates.DateFormatter('%H')
                axes.xaxis.set_major_locator(hours)
                axes.xaxis.set_major_formatter(date_fmt)        
                min_date = lxarray['time'].values.min()
                max_date = lxarray['time'].values.max()
                axes.set_xlim(min_date.astype('datetime64[D]'), max_date.astype('datetime64[D]') + np.timedelta64(1,'D'))            
                axes.set_ylim(plt_conf['y_min'], plt_conf['y_max'])
                plt.grid(True)
                axes.set_xlabel('Time, $[UTC]$')
                axes.set_ylabel('Altitude, $[km]$')
                cb.ax.set_ylabel('%s, %s' % (lxarray[_key].attrs['long_name'], lxarray[_key].attrs['units']))
                    
                # title
                # ----------------------------------------------------------------------------
                datestr = lxarray.time[0].min().values.astype('str').split('T')[0]
                plt_conf['title1'] = '%s %s' % (lxarray.attrs['instrument_id'], channels[_key])
                plot.title1(plt_conf['title1'], plt_conf['coeff'])
                plot.title2(datestr, plt_conf['coeff'])
                plot.title3('{} ({:.1f}N, {:.1f}E)'.format(lxarray.attrs['site_location'], float(lxarray['lat'].values), float(lxarray['lon'].values)), plt_conf['coeff'])

                # logo
                # ----------------------------------------------------------------------------
                plot.add_GFAT_logo([0.85, 0.01, 0.15, 0.15])

                debugging = False

                if debugging:
                    plt.show()
                else:            
                    #create output folder
                    # --------------------------------------------------------------------
                    year = datestr[0:4]
                    fulldirpath = os.path.join(figdirectory, channels[_key], year) 
                    if np.logical_not(os.path.exists(fulldirpath)):
                        mkpath(fulldirpath)
                        print('fulldirpath created: %s' % fulldirpath)                
                    figstr = '%s_%s_%s-%s_%s.png' % (lxarray.attrs['lidarNick'], lxarray.attrs['dataversion'], signals[_key], channels[_key], datestr.replace('-',''))
                    finalpath = os.path.join(fulldirpath, figstr)
                    print('Saving %s' % finalpath)
                    plt.savefig(finalpath, dpi=100, bbox_inches = 'tight')
                    if os.path.exists(finalpath):
                        print('Saving %s...DONE!' % finalpath)
                    else:
                        print('Saving %s... error!' % finalpath)
                    plt.close()
    return

def daily_quicklook(filelist, dcfilelist, figdirectory):
    """
    Formatted daily quicklook of RPG Cloud Radar measurements.
    Inputs:
    - filelist: List of radar files (i.e, '/drives/c/*ZEN*.LC?') (str)
    - figdirectory: Array of numbers corresponding to the moment of the Doppler spectra. (integer)
    Outputs:
    - None
    """
    plt_conf = {}
    plt_conf['gapsize'] = 'default'
    plt_conf['y_min'] = 0
    plt_conf['y_max'] = 14
    plt_conf['coeff'] = 2    
    # channels2plot = (0, 2, 4, 6)
    channels2plot = (0,)
    plot_lidar_channels(filelist, dcfilelist, channels2plot, plt_conf, figdirectory)

def date_quicklook(dateini, dateend, path1a='GFATserver', figpath='GFATserver'):
    """
    Formatted daily quicklook of lidar measurements for hierarchy GFAT data.
    Inputs:
    - path1a: path where 1a-level data are located.
    - figpath: path where figures are saved.
    - Initial date [yyyy-mm-dd] (str). 
    - Final date [yyyy-mm-dd] (str).
        
    Outputs: 
    - None
    """        

    if path1a == 'GFATserver':
        path1a = '/mnt/NASGFAT/datos/MULHACEN/1a'

    if figpath == 'GFATserver':
        figserverpath = '/mnt/NASGFAT/datos/MULHACEN/quicklooks'        

    inidate = dt.datetime.strptime(dateini, '%Y%m%d')
    enddate = dt.datetime.strptime(dateend, '%Y%m%d')

    period = enddate - inidate
    
    for _day in range(period.days + 1):
        current_date = inidate + dt.timedelta(days=_day)            
        filename = 'mhc_1a_Prs*%s*.nc' % (dt.datetime.strftime(current_date, '%y%m%d'))
        filelist = os.path.join(path1a, '%d' % current_date.year, '%02d' % current_date.month, '%02d' % current_date.day, filename)        
        
        dcfilename = 'mhc_1a_Pdc*%s*.nc' % (dt.datetime.strftime(current_date, '%y%m%d'))
        dcfilelist = os.path.join(path1a, '%d' % current_date.year, '%02d' % current_date.month, '%02d' % current_date.day, dcfilename)        
        if figpath == 'GFATserver':
            figdirectory = figserverpath

        daily_quicklook(filelist, dcfilelist, figdirectory)
    

def main():
    parser = argparse.ArgumentParser(description="usage %prog [arguments]")    
    parser.add_argument("-i", "--initial_date",
        action="store",
        dest="dateini",
        required=True,
        help="Initial date [example: '20190131'].")         
    parser.add_argument("-e", "--final_date",
        action="store",
        dest="dateend",
        default=".",
        help="Final date [example: '20190131'].")
    parser.add_argument("-d", "--datadir",
        action="store",
        dest="path1a",
        default="GFATserver",
        help="Path where date-hierarchy files are located [example: '~/data/1a'].")
    parser.add_argument("-f", "--figuredir",
        action="store",
        dest="figpath",
        default="GFATserver",
        help="Path where figures will be saved [example: '~/radar/quicklooks'].")
    args = parser.parse_args()

    date_quicklook(args.dateini, args.dateend, path1a=args.path1a, figpath=args.figpath)

if __name__== "__main__":
    main()
