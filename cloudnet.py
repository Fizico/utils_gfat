#!/usr/bin/env python

"""
function for reading files from CLOUDNET

Categorice_reader: line 17
Classification_reader: line 65
cloudnetQuicklook: 131
"""

import os
import numpy as np
import netCDF4 as nc
import datetime as dt
import matplotlib as mpl
import matplotlib.pyplot as plt
import pdb

__author__ = "Bravo-Aranda, Juan Antonio"
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Bravo-Aranda, Juan Antonio"
__email__ = "jabravo@ugr.es"
# __status__ = "Production"


def categorice_reader(list_files):
    """read data from netCDF files

    Designed to work with netcdf list_files associated to one single day
    """
    print('Reading netCDF files: %s' % list_files)    
    data = {}
    
    if list_files:
        var2load = ['lwp', 'Z', 'beta', 'mean_zbeta', 'temperature', 'v',
                    'pressure', 'specific_humidity', 'rainrate']
    
        # open all files
        nc_ids = [nc.Dataset(file_) for file_ in list_files]

        # localization of instrument
        data['lat'] = nc_ids[0].variables['latitude'][:]
        data['lon'] = nc_ids[0].variables['longitude'][:]
        data['alt'] = nc_ids[0].variables['altitude'][:]
        data['location'] = nc_ids[0].location
        
        # read alt (no need to concatenate)
        data['height'] = nc_ids[0].variables['height'][:]
        data['model_height'] = nc_ids[0].variables['model_height'][:]
    
        # wavelength (no need to concatenate)
        tmp = nc_ids[0].variables['lidar_wavelength'][:]
        data['wavelength'] = tmp 
        data['wavelength_units'] = nc_ids[0].variables['lidar_wavelength'].units  
    
        # read time 
        units = nc_ids[0].variables['time'].units  # HOURS SINCE %Y-%m-%d 00:00:00 +0:00
        tmp = [nc.num2date(nc_id.variables['time'][:], units) for nc_id in nc_ids]
        data['raw_time'] = np.concatenate(tmp)
    
        # check if any data available
        # print(date)
        # time_filter = (data['raw_time'] >= date) & (data['raw_time'] < date + dt.timedelta(days=1))
        # if not np.any(time_filter):
        #     return None    
        
        for var in var2load:
            tmp = [nc_id.variables[var][:] for nc_id in nc_ids]
            data[var] = np.ma.filled(np.concatenate(tmp, axis=0), fill_value=np.nan)
            # It is assumed that properties do not change along netcdf files for the same day
            data[var][data[var] == nc_ids[0][var].missing_value] = np.nan
               
        # Change name to fit real array content
        data['dBZe'] = data['Z'].copy()
        
        # close all files
        [nc_id.close() for nc_id in nc_ids]

    return data    


def classification_reader(list_files):
    """read data from netCDF files

    Designed to work with netcdf list_files associated to one single day
    """
    """
    0: Clear sky
    1: Cloud liquid droplets only
    2: Drizzle or rain
    3: Drizzle or rain coexisting with cloud liquid droplets
    4: Ice particles
    5: Ice coexisting with supercooled liquid droplets
    6: Melting ice particles
    7: Melting ice particles coexisting with cloud liquid droplets
    8: Aerosol particles, no cloud or precipitation
    9: Insects, no cloud or precipitation
    10: Aerosol coexisting with insects, no cloud or precipitation
    """
    print('Reading netCDF files: %s' % list_files)            

    data = {}
    
    if list_files:
        var2load = ['cloud_base_height', 'cloud_top_height', 'target_classification']
        # open all files
        nc_ids = [nc.Dataset(file_) for file_ in list_files]
    
        # localization of instrument
        data['lat'] = nc_ids[0].variables['latitude'][:]
        data['lon'] = nc_ids[0].variables['longitude'][:]
        data['alt'] = nc_ids[0].variables['altitude'][:]
        data['location'] = nc_ids[0].location
        
        # read alt (no need to concantenate)
        data['height'] = nc_ids[0].variables['height'][:]
    
        # read time 
        units = nc_ids[0].variables['time'].units #'days since %s' % dt.datetime.strftime(date, '%Y-%m-%d %H:%M:%S')
        tmp = [nc.num2date(nc_id.variables['time'][:], units) for nc_id in nc_ids]
        data['raw_time'] = np.concatenate(tmp)
    
        # check if any data available
        # print(date)
        # time_filter = (data['raw_time'] >= date) & (data['raw_time'] < date + dt.timedelta(days=1))
        # if not np.any(time_filter):
        #     return None    
        
        for var in var2load:        
            tmp = [nc_id.variables[var][:] for nc_id in nc_ids]
            data[var] = np.ma.filled(np.concatenate(tmp, axis=0), fill_value=np.nan)
            # It is assumed that properties do not change along netcdf files for the same day
            if 'missing_value' in nc_ids[0][var].ncattrs():
                data[var][data[var] == nc_ids[0][var].missing_value] = np.nan
                
        # close all files
        [nc_id.close() for nc_id in nc_ids]

    return data 


def plotQuicklook(data, plt_conf, saveImageFlag):   
    """
    Inputs: 
    - data: from categorice_reader()
    - plt_conf: plot configuration dictionary as follows:
        plt_conf =  {
            'mainpath': "Y:\\datos\\CLOUDNET\\juelich\\quicklooks",
            'coeff': COEFF,
            'gapsize': HOLE_SIZE,
            'dpi': dpi,
            'fig_size': (16,5),
            'font_size': 16,
            'y_min': 0,
            'y_max': range_limit,
            'rcs_error_threshold':1.0, }
    - saveImageFlag [Boolean]: to save png-figure or print in command screen.
    """          
    var2plot = {0: 'dBZe', 1: 'v', 2: 'beta'} #, 2: 'sigma' , 3: 'sigma', 4: 'kurt'                        
    #Dictionary for the vmax and vmin of the plot
    Vmin = {0: -55, 1: -1.5, 2:0} # , 2: 0 , 3: -3, 4: -3
    Vmax = {0: -20, 1: 1.5, 2: 10} #, 2: 5 , 3: 3, 4: 3
    Vn = {0: 16, 1: 7, 2: 10}
    scale = {0: 1, 1: 1, 2: 1e6}
    titleStr = {0: 'Reflectivity', 1: 'Vertical mean velocity', 2: 'Backscatter coeff.'} #, 2: 'spectral width'
    cblabel = {0: '$Z_e, dBZe$', 1: '$V_m, m/s$', 2: r'$\beta$, $Mm^-1$'} #, 2: 'spectral width'
    datestr = data['raw_time'][0].strftime('%Y%m%d')
    for idx in var2plot.keys():            
        _var = var2plot[idx]
        #print(idx)
        print(_var)        
        _fig, _axes = plt.subplots(nrows=1, figsize=(15,6))
        _axes.set_facecolor('whitesmoke')
        cmap = mpl.cm.jet
        bounds = np.linspace(Vmin[idx],Vmax[idx], Vn[idx])
#        norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
        range_km = data['height']/1000.
        q = _axes.pcolormesh(data['raw_time'], range_km, scale[idx]*data[_var].T,
                            cmap=cmap,
                            vmin=Vmin[idx],
                            vmax=Vmax[idx],)
        q.cmap.set_over('white')
        q.cmap.set_under('darkblue')
        cb = plt.colorbar(q, ax=_axes,
                          ticks=bounds,
                          extend='max')
        cb.set_label(cblabel[idx])        
        _axes.set_xlabel('Time, UTC')
        _axes.set_ylabel('Height, Km asl')
        _axes.set_title('%s on  %s' % (titleStr[idx], datestr))
        xmin = dt.datetime.strptime(datestr, '%Y%m%d')
        xmax = xmin + dt.timedelta(days=1)
        _axes.set_xlim(xmin, xmax)
        _axes.set_ylim(0, 2)
        if saveImageFlag:            
            figstr = '%s_%s_%s.png' % ('cloudnet', var2plot[idx], datestr)
            finalpath = os.path.join(plt_conf['mainpath'], _var, figstr)              
            print('Saving %s' % finalpath)
            final_dir_path = os.path.split(finalpath)[0]
            if not os.path.exists(final_dir_path):
                os.makedirs(final_dir_path)
            plt.savefig(finalpath, dpi=100, bbox_inches='tight')
            if os.path.exists(finalpath):
                print('Saving %s...DONE!' % finalpath)
            else:
                print('Saving %s... error!' % finalpath)
            plt.close(_fig)
        else:
            plt.show()


def plotLWP(data, mainpath, saveImageFlag):   
    """
    Inputs: 
    - data: from categorice_reader()
    - mainpath: "Y:\\datos\\CLOUDNET\\juelich\\quicklooks",
    - saveImageFlag [Boolean]: to save png-figure or print in command screen.
    """                  
    datestr = data['raw_time'][0].strftime('%Y%m%d')    
    fig, axes = plt.subplots(nrows=1, figsize=(15,6))
    axes.set_facecolor('whitesmoke')
    plt.plot(data['raw_time'], data['lwp'])
    axes.set_xlabel('Time, UTC')
    axes.set_ylabel('LWP, $g/m^2$')
    axes.set_title('LWP on  %s' % datestr)
    xmin = dt.datetime.strptime(datestr, '%Y%m%d')
    xmax = xmin + dt.timedelta(days=1)
    axes.set_xlim(xmin, xmax)
    if saveImageFlag:
        figstr = '%s_%s_%s.png' % ('cloudnet', 'lwp', datestr)
        finalpath = os.path.join(mainpath, 'LWP', figstr)              
        print('Saving %s' % finalpath)
        final_dir_path = os.path.split(finalpath)[0]
        if not os.path.exists(final_dir_path):
            os.makedirs(final_dir_path)
        plt.savefig(finalpath, dpi=100, bbox_inches='tight')
        if os.path.exists(finalpath):
            print('Saving %s...DONE!' % finalpath)
        else:
            print('Saving %s... error!' % finalpath)
        plt.close(fig)
    else:
        plt.show()