#!/usr/bin/env python

import os
import sys
import glob
import shutil
import platform
import argparse
import numpy as np
import datetime as dt
from lidar import getTP
from atmospheric_lidar.scripts import licel2scc
from distutils.dir_util import mkpath

__author__ = "Bravo-Aranda, Juan Antonio"
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Bravo-Aranda, Juan Antonio"
__email__ = "jabravo@ugr.es"
__status__ = "Production"

def date_from_filename(filelist):
    """
    It takes the date from the file name of licel files.
    Input:
    filelist: list of licel-formatted files (list, string). 
    Output:
    datelist: list of dates (list, datetime). 
    """

    datelist = []
    if filelist:            
        for _file in filelist:        
            body = _file.split('.')[0]
            #tail = _file.split('.')[1]
            #hour = int(body[-2:])
            day = int(body[-4:-2])
            month = body[-5:-4]
            try:
                month = int(month)
            except:
                if month == 'A':
                    month = 10
                elif month == 'B':
                    month = 11
                elif month == 'C':
                    month = 12
            year = int(body[-7:-5]) + 2000      
            #print('from body %s: the date %s-%s-%s' % (body, year, month, day))      
            cdate = dt.datetime(year, month, day)
            datelist.append(cdate)
    else:
        print('Filelist is empty.')
        datelist = None
    return datelist

def move2odir(source_filename, destination, overwrite = True):
    """
    It moves the file named source_filename in local directory to the absolute path file 'moved_filepath'.
    Input:
    source_filename: filename (string). 
    destination: destination path (string). 
    Output:
    sent: control variable. True=sent; False=not sent. (boolean).
    """
    sent = False    
    print('Moving %s to %s' % (source_filename, destination))
    try:        
        if not os.path.isdir(destination):
            mkpath(destination)
        if overwrite:          
            moved_file = shutil.move(source_filename, os.path.join(destination, source_filename))
        else:
            moved_file = shutil.move(source_filename, destination)
        if os.path.isfile(moved_file):
            print('Moving file to user-defined output directory: %s... DONE!' % source_filename)
            sent = True
        else:
            print('Moving file to user-defined output directory: %s...ERROR!' % source_filename)            
    except:
        print('Moving file to user-defined output directory: %s...ERROR!' % source_filename)
    return sent

def directory2scc(dirpath, nicklidar = 'mhc', measure_type = 'RS', scc_code = 436, hour_resolution = 3, outputdir='GFATserver'):
    """
    Input: 
    dirpath: Directory containing licel files. (str)
    nicklidar: Nickname of the lidar system (eg: 'mhc', 'vlt') (str)
    measure_type = Type of measure according to the GFAT data hierarchy ('RS': regular signal, 'HF': High frequency signal, 'TC': telecover, 'DP': depolarization, 'OT': other measure). Default: 'RS' (str).
    ssc_code: scc configuration code (e.g., 436) (int)
    hour_resolution: measurement period will be splitted in time slots in number of hours (default:3 ) (int)
    outputdir: directory where scc files are saved. Default: 'GFATserver' means copy files to the GFAT NAS.
    Output:    
    """        
    scc_config_directory = '/usr/local/anaconda3/lib/python3.7/site-packages/utils_gfat/scc_configFiles'
    mdict={'01':'1', '02':'2', '03':'3', '04':'4', '05':'5', '06':'6', '07':'7', '08':'8', '09':'9', '10': 'A', '11': 'B', '12': 'C'}
    lidarcode = {'mhc': '', 'vlt': '02_'}
    lidarname = {'mhc': 'MULHACEN', 'vlt': 'VELETA'}
    stationID = 'gr'    

    scc_config = os.path.join(scc_config_directory,'%s_parameters_scc_%d.py' % (nicklidar, scc_code))
    if not os.path.isfile(scc_config):
        print('ERROR: scc config file not found: %s ' % scc_config)
        sys.exit(1)

    if outputdir == 'GFATserver':
        outputdir = os.path.join('/mnt/NASGFAT/datos', lidarname[nicklidar], 'scc%d' % scc_code, 'input_%02dhoras' % hour_resolution)
        if platform.system() == 'Windows':
            outputdir = os.path.join('Y:', 'datos', lidarname[nicklidar], 'scc%d' % scc_code, 'input_%02dhoras' % hour_resolution)
    if not os.path.isdir(outputdir):
        print('ERROR: output directory not found: %s ' % outputdir)
        return
        
    if not os.path.isdir(dirpath):
        print('ERROR: User-provided directory with licel daya not found: %s ' % dirpath)
        return

    #Defining dark measurement directory
    dcpattern = os.path.join(os.path.dirname(dirpath), os.path.basename(dirpath).replace(measure_type, 'DC'), 'R*')
    #print(dcpattern)                
    
    #List with dates of measurements
    datelist = date_from_filename(glob.glob(os.path.join(dirpath, 'R*')))
    datelist.sort()        
    #Dates in the folder
    dates = np.unique([_date.date() for _date in datelist])    
    
    if not np.asarray([np.asarray(np.diff(dates)) == dt.timedelta(days=1)]).all():        
        print('ERROR: non consecutive days in the same measurment session folder. Please, check %s' % dirpath)
        return
        
    #Running licel converter for each date for each time interval     
    for _date in dates:        
        print('Running day: %s' % _date)
        datestr = dt.datetime.strftime(_date, '%Y%m%d')
        year4 = dt.datetime.strftime(_date, '%Y')
        year = dt.datetime.strftime(_date, '%y')                
        month = dt.datetime.strftime(_date, '%m')        
        day = dt.datetime.strftime(_date, '%d')
        bodytype = 'R*%s%s%s' %(year, mdict[month], day)        
        for idnumber, hour in enumerate(np.arange(0,24,hour_resolution)):
            bottom = hour
            up = bottom + (hour_resolution - 1)                
            filelist = []
            for _hour in np.arange(bottom,up + 1):
                filetype = '%s%02d*.*' % (bodytype, _hour)
                print('Looking for files with shape: %s' % os.path.join(dirpath, filetype))
                found = glob.glob(os.path.join(dirpath, filetype)) 
                if found:                    
                    filelist = filelist + found
            print('Measurements found in the time interval [%d-%d]: %d' %(bottom, up, len(filelist)))
            #Once the filelist contains the files within the given time interval (loop), it is converted to scc format.
            if filelist:
                measurementID = '%s%s%s%02d' % (datestr, stationID, lidarcode[nicklidar],idnumber)
                temperature, pressure = getTP(filelist[0]) #Take temperature and pressure from the first file.                
                CustomLidarMeasurement = licel2scc.create_custom_class(scc_config, use_id_as_name = True, temperature=temperature, pressure=pressure)
                print('Creating file: %s...' % measurementID)                
                licel2scc.convert_to_scc(CustomLidarMeasurement, filelist, dcpattern, measurementID, idnumber)
                fileextension = '%s.nc' % measurementID
                if os.path.isfile(fileextension):
                    print('Creating file: %s... DONE!' % measurementID)
                    if outputdir:
                        outputpath = os.path.join(outputdir, year4, month, day)
                        move2odir(fileextension, outputpath)
                else:
                    print('Creating file: %s... ERROR! File not found.' % measurementID)

def run_period_scc_converter(dateini, dateend, nicklidar = 'mhc', measure_type = 'RS', scc_code = 436, hour_resolution = 3, outputdir='GFATserver'):
    """
    It converts to scc format the session folders between dateini and dateend of an specific lidar system. 
    Input:    
    dateini: First date. Format yyyymmdd (str)
    dateend: Last date. Format yyyymmdd (str)
    nicklidar: Nickname of the lidar system (eg: 'mhc', 'vlt') (str)
    measure_type = Type of measure according to the GFAT data hierarchy ('RS': regular signal, 'HF': High frequency signal, 'TC': telecover, 'DP': depolarization, 'OT': other measure). Default: 'RS' (str). Use 'all' to run both 'RS' and 'HF'.
    ssc_code: scc configuration code (e.g., 436) (int)
    hour_resolution: measurement period will be splitted in time slots in number of hours (default:3 ) (int)
    outputdir: directory where scc files are saved. Default: 'GFATserver' means copy files to the GFAT NAS.
    Output:
    """
    lidarname = {'mhc': 'MULHACEN', 'vlt': 'VELETA'}
    sdate = dt.datetime.strptime(dateini, '%Y%m%d')
    edate = dt.datetime.strptime(dateend, '%Y%m%d')
    delta = edate - sdate       # as timedelta
    
    if measure_type == 'all':
        measure_types = ['RS', 'HF']
    else:
        measure_types = [measure_type]
    
    #main directory
    mdir = os.path.join('/mnt/NASGFAT/datos', lidarname[nicklidar], '0a')
    if platform.system() == 'Windows':
        mdir = os.path.join('Y:', 'datos', lidarname[nicklidar], '0a')            
   
    for i in range(delta.days + 1):
        _date = sdate + dt.timedelta(days=i)        
        datestr = dt.datetime.strftime(_date, '%Y%m%d')
        day, month, year = (dt.datetime.strftime(_date, '%d'), dt.datetime.strftime(_date, '%m'), dt.datetime.strftime(_date, '%Y'))
        print('Date of loop (%d of %d): %s' % (i+1, delta.days + 1, datestr))
        if os.path.isdir(os.path.join(mdir, year, month, day)):
            for _mtype in measure_types:
                dirs = glob.glob(os.path.join(mdir, year, month, day, '%s_%s_*' % (_mtype, datestr)))
                dirs.sort()
                print('Folders found: %s' % dirs)
                if dirs:
                    print(dirs)
                    for _dir in dirs:
                        print('Running directory: %s' % _dir)
                        directory2scc(_dir, nicklidar = nicklidar, measure_type = _mtype, scc_code = scc_code, hour_resolution = hour_resolution, outputdir=outputdir)
                else:
                    print('Folder not found: %s' % os.path.join(mdir, year, month, day))

def main():
    parser = argparse.ArgumentParser(description="usage %prog [arguments]")    
    parser.add_argument("-i", "--initial_date",
        action="store",
        dest="dateini",
        required=True,
        help="Initial date [example: '20190131'].")         
    parser.add_argument("-e", "--final_date",
        action="store",
        dest="dateend",
        required=True,        
        help="Final date [example: '20190131'].")
    parser.add_argument("-n", "--nicklidar",
        action="store",
        dest="nicklidar",        
        default="mhc",
        help="Nickname of lidar system [example: 'mhc', 'vlt']. Default: 'mhc'.")
    parser.add_argument("-m", "--measure_type",
        action="store",
        dest="measure_type",
        default="RS",
        help="Type of measurement [example: 'RS', 'HF'].")
    parser.add_argument("-s", "--scc_code",
        action="store",
        dest="scc_code",
        default=436,
        help="SCC configuration code [example: 436, 403].")
    parser.add_argument("-r", "--hour_resolution",
        action="store",
        dest="hour_resolution",
        default=3,
        help="measurement period will be splitted in time slots in number of hours (default:3 ) (int)")
    parser.add_argument("-o", "--output_directory",
        action="store",
        dest="output_directory",
        default='GFATserver',
        help="directory where scc files are saved. Default: 'GFATserver' means copy files to the GFAT NAS.")
    args = parser.parse_args()

    run_period_scc_converter(args.dateini, args.dateend, nicklidar = args.nicklidar, measure_type = args.measure_type, scc_code = args.scc_code, hour_resolution = args.hour_resolution, outputdir= args.output_directory)

if __name__== "__main__":
    main()